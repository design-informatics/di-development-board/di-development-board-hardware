EAGLE AutoRouter Statistics:

Job           : /Users/dmrust/OneDrive - University of Edinburgh/Work/Creative Electronics/DI PCB/DI PCB v1.brd

Start at      : 10:18:12 (10/09/2020)
End at        : 10:20:24 (10/09/2020)
Elapsed time  : 00:02:07

Signals       :    40   RoutingGrid: 5.5 mil  Layers: 2
Connections   :   154   predefined:  20 ( 0 Vias )

Router memory :   3219156

Passname          : TopRouter     Route Optimize1 Optimize2 Optimize3 Optimize4 Optimize5 Optimize6 Optimize7 Optimize8 Optimize9Optimize10Optimize11Optimize12

Time per pass     :  00:00:20  00:00:09  00:00:09  00:00:09  00:00:09  00:00:08  00:00:08  00:00:08  00:00:08  00:00:08  00:00:08  00:00:07  00:00:08  00:00:08
Number of Ripups  :         0         0         0         0         0         0         0         0         0         0         0         0         0         0
max. Level        :         0         0         0         0         0         0         0         0         0         0         0         0         0         0
max. Total        :         0         0         0         0         0         0         0         0         0         0         0         0         0         0

Routed            :        45       134       134       134       134       132       132       132       132       134       132       132       132       132
Vias              :         0       104       112       115       116       115       115       115       107       105       102       102       102       102
Resolution        :    42.2 %   100.0 %   100.0 %   100.0 %   100.0 %    98.7 %    98.7 %    98.7 %    98.7 %   100.0 %    98.7 %    98.7 %    98.7 %    98.7 %

Final             : 97.4% finished. Polygons may have fallen apart.
